package t20kdc.libofflinepuzzlesolver.sudoku;

import java.util.concurrent.atomic.AtomicBoolean;

import t20kdc.libofflinepuzzlesolver.genlog.GML;
import t20kdc.libofflinepuzzlesolver.genlog.TreeWalkerSimple;

/**
 * This class is for testing. It's kept here for organizational simplicity.
 */
public class STMain {
    public static void main(String[] args) {
        GML.logEnabled = false;
        SudokuRoot.fudgeEnabledGlobally = false;
        // There's so many ways to solve an empty board, it's kind of silly.
        test("empty", new String[] {
                "___ ___ ___",
                "___ ___ ___",
                "___ ___ ___",

                "___ ___ ___",
                "___ ___ ___",
                "___ ___ ___",

                "___ ___ ___",
                "___ ___ ___",
                "___ ___ ___",
        }, true);
        // This board contains a contradiction, two 1s in the same 3x3 square.
        test("contradict33", new String[] {
                "___ ___ ___",
                "___ ___ ___",
                "___ ___ ___",

                "___ ___ ___",
                "___ ___ ___",
                "___ ___ ___",

                "___ ___ 1__",
                "___ ___ _1_",
                "___ ___ ___",
        }, false);
        // This board contains a contradiction, two 1s in the same row.
        test("contradictR", new String[] {
                "___ ___ ___",
                "___ ___ ___",
                "___ ___ ___",

                "___ ___ ___",
                "_1_ ___ _1_",
                "___ ___ ___",

                "___ ___ ___",
                "___ ___ ___",
                "___ ___ ___",
        }, false);
        // This board contains a contradiction, two 1s in the same column.
        test("contradictC", new String[] {
                "___ ___ ___",
                "___ _1_ ___",
                "___ ___ ___",

                "___ ___ ___",
                "___ ___ ___",
                "___ ___ ___",

                "___ ___ ___",
                "___ _1_ ___",
                "___ ___ ___",
        }, false);
        // This board's top-right arm and bottom-left arm forces the lower-right block into a specific configuration.
        // That in turn forces the lower-left block into more or less another configuration.
        // That causes the lower-middle block to fail.
        // See comments.
        test("cornered", new String[] {
                "___ ___ 936",
                "___ ___ 825",
                "___ ___ 714",

                "___ ___ 369",
                "___ ___ 258",
                "___ ___ 147",

                "752 _8_ ___", // 752 _8_ 471
                "___ _41 ___", // 963 _41 582 (left 3 order arbitrary)
                "481 572 ___", // 481 572 693

                // The problem is:
                // _8_ <- row can have: 369
                // _41 <- row can't have anything, causing the failure
                // 572
                // block needs: 369
        }, false);
        // This board isn't solvable, as there's no place to put a 2 in the centre 3x3 square.
        // Disabled right now because the solver gets stuck.
        test("crossroads", new String[] {
                "___ ___ ___",
                "___ 1_2 ___",
                "___ ___ ___",

                "_1_ ___ _2_",
                "___ _1_ ___",
                "_2_ ___ _1_",

                "___ ___ ___",
                "___ 2_1 ___",
                "___ ___ ___",
        }, false);
    }
    private static void test(String boardName, String[] board, boolean solvable) {
        System.out.println();
        System.out.println("Running test " + boardName + " (sv " + solvable + ")");
        if (testCore(board) != solvable)
            throw new RuntimeException("Board solvability failure");
        System.out.println();
    }
    public static boolean testCore(String[] board) {
        SudokuRoot.State st = new SudokuRoot.State();
        int idx = 0;
        for (int y = 0; y < 9; y++) {
            for (int i = 0; i < 9; i++) {
                int ie = i + (i / 3);
                char ch = board[y].charAt(ie);
                if (ch == ' ') {
                    throw new RuntimeException("Test board format issue");
                } else if (ch != '_') {
                    int choice = ch - '1';
                    // other
                    if (!st.wouldBeValid(idx, choice)) {
                        System.out.println("Solvability failure, " + i + " " + y + ", early");
                        System.out.println("RC " + Integer.toHexString(st.reasonFor(idx, choice)));
                        return false;
                    }
                    st = new SudokuRoot.State(st, idx, choice);
                }
                idx++;
            }
        }
        TreeWalkerSimple<SudokuRoot.State> srs = new TreeWalkerSimple<>(new SudokuRoot(), new AtomicBoolean());
        SudokuRoot.State sto = srs.walk(st);
        boolean res = true;
        if (sto == null) {
            System.out.println("Solvability failure, late");
            res = false;
            sto = srs.getHighestImportance();
        }
        if (sto != null) {
            for (int y = 0; y < 9; y++) {
                for (int x = 0; x < 9; x++) {
                    int ch = sto.getChoice(x + (y * 9));
                    if (ch == -1) {
                        System.out.print('_');
                    } else {
                        System.out.print((char) ('1' + ch));
                    }
                }
                System.out.print('\n');
            }
        }
        return res;
    }
}
