package t20kdc.libofflinepuzzlesolver.sudoku;

import java.util.Arrays;
import java.util.Collection;

import t20kdc.libofflinepuzzlesolver.genlog.TreeWalkerRoot;

public class SudokuRoot implements TreeWalkerRoot<SudokuRoot.State> {
    public static boolean fudgeEnabledGlobally = true;
    private static final int[] squareOfs = {
            0, 1, 2,
            9, 10, 11,
            18, 19, 20
    };

    @Override
    public void initState(State init) {
    }
    @Override
    public void visit(State node, Collection<State> intern) {
        if (node.stateEpicFailCheckAlternator)
            if (node.checkForEpicFailure() != 0)
                return;
        int bestVisit = -1;
        int bestVisitChoices = 10;
        // in case of error, lower maximum choices so it completes in reasonable time,
        // but 'allow it to continue' past errors
        boolean walkaboutsLockout = false;
        int walkaboutsCounter = 1;
        for (int i = 0; i < 9 * 9; i++) {
            if (node.completion[i])
                continue;
            int c = node.getChoiceCount(i);
            if (node.enableWalkabouts && (c == 0)) {
                walkaboutsLockout = true;
                continue;
            }
            if ((c < bestVisitChoices) || ((c == bestVisitChoices) && fudge(i))) {
                bestVisit = i;
                bestVisitChoices = c;
            }
        }
        if (bestVisit == -1)
            return;
        if (bestVisitChoices == 0)
            return;
        for (int i = 0; i < 9; i++) {
            if (node.wouldBeValid(bestVisit, i)) {
                intern.add(new State(node, bestVisit, i));
                if (walkaboutsLockout)
                    if (--walkaboutsCounter <= 0)
                        return;
            }
        }
    }

    private boolean fudge(int i) {
        // This isn't essential, it just adds some fun, at least in theory.
        return fudgeEnabledGlobally && (i + (System.currentTimeMillis() % 16)) == 0;
    }

    @Override
    public int getImportanceOf(State node) {
        return node.completionAmount;
    }
    @Override
    public boolean isEndpoint(State node) {
        return node.completionAmount == 9 * 9;
    }

    public static final class State {
        // 9X 9Y 9D validity
        // Integers given here imply reason of non-validity, and therefore zero actually means valid.
        public final int[] validity;
        // 9x9 completion
        public final boolean[] completion;
        public final int completionAmount;
        public boolean enableWalkabouts = false;
        public final boolean stateEpicFailCheckAlternator;
        public State() {
            validity = new int[9 * 9 * 9];
            completion = new boolean[9 * 9];
            completionAmount = 0;
            stateEpicFailCheckAlternator = false;
        }
        public State(State previous, int where, int choice) {
            enableWalkabouts = previous.enableWalkabouts;
            stateEpicFailCheckAlternator = !previous.stateEpicFailCheckAlternator;
            validity = previous.validity.clone();
            completion = previous.completion.clone();
            if (completion[where])
                throw new RuntimeException("double-completion");
            if ((choice < 0) || (choice > 8))
                throw new RuntimeException("oor choice " + where + " : " + choice);
            if (validity[(where * 9) + choice] != 0)
                throw new RuntimeException("invalid choice");
            // Alright, firstly, disable all validity on this square
            int vReasonHere = Reason.CELL_ALREADY_WRITTEN | where;
            for (int i = 0; i < 9; i++)
                validity[(where * 9) + i] = vReasonHere;
            // Then propagate non-validity to other squares
            int whereX = where % 9;
            int whereY = where / 9;
            int wcXB = (whereX * 9) + choice;
            int wcYB = (whereY * 9 * 9) + choice;
            int wSB = ((whereX / 3) * 3) + ((whereY / 3) * 3 * 9);
            int wcSB = (wSB * 9) + choice;
            int vReasonX = Reason.CONFLICT_ON_COL | where;
            int vReasonY = Reason.CONFLICT_ON_ROW | where;
            int vReasonS = Reason.CONFLICT_ON_SQR | where;
            for (int i = 0; i < 9; i++) {
                validity[wcXB + (i * 9 * 9)] = vReasonX;
                validity[wcYB + (i * 9)] = vReasonY;
                validity[wcSB + (squareOfs[i] * 9)] = vReasonS;
            }
            // Finally, re-enable validity on this square and update completion log
            validity[(where * 9) + choice] = 0;
            completion[where] = true;
            completionAmount = previous.completionAmount + 1;
        }
        public boolean wouldBeValid(int where, int choice) {
            return validity[(where * 9) + choice] == 0;
        }
        public int reasonFor(int where, int choice) {
            return validity[(where * 9) + choice];
        }
        public int getChoiceCount(int where) {
            int c = 0;
            for (int i = 0; i < 9; i++)
                if (wouldBeValid(where, i))
                    c++;
            return c;
        }
        public int getChoice(int where) {
            int c = -1;
            for (int i = 0; i < 9; i++) {
                if (wouldBeValid(where, i)) {
                    if (c != -1)
                        return -1;
                    c = i;
                }
            }
            return c;
        }

        /**
         * An interesting oddity comes up with the interaction between the 3x3 grid square checks and other checks.
         * This is demo'd in the crossroads test.
         * This is because digits can be placed in a 'conspiratorial' way that causes the constraint test to fail.
         * Theoretically this could also occur by accident in the algorithm.
         * So this check makes absolutely sure that every single group is valid.
         * This check is possibly a bit expensive, so it's only done on every alternating generation normally.
         * @return If non-zero, this state has epically failed. Reason is given.
         */
        public int checkForEpicFailure() {
            for (int i = 0; i < 9; i++) {
                for (int d = 0; d < 9; d++) {
                    boolean rowValid = false;
                    boolean colValid = false;
                    boolean sqrValid = false;
                    for (int j = 0; j < 9; j++) {
                        int rowP = j + (i * 9);
                        int colP = (j * 9) + i;
                        int sqrP = squareOfs[j] + (squareOfs[i] * 3);
                        if (wouldBeValid(rowP, d))
                            rowValid = true;
                        if (wouldBeValid(colP, d))
                            colValid = true;
                        if (wouldBeValid(sqrP, d))
                            sqrValid = true;
                    }
                    if (!rowValid)
                        return Reason.NO_SPACE_ON_ROW | (i * 9) | (d << Reason.SHIFT_DIGIT);
                    if (!colValid)
                        return Reason.NO_SPACE_ON_COL | i | (d << Reason.SHIFT_DIGIT);
                    if (!sqrValid)
                        return Reason.NO_SPACE_ON_SQR | (squareOfs[i] * 3) | (d << Reason.SHIFT_DIGIT);
                }
            }
            return 0;
        }
    }

    /**
     * Reason values describe a 'conflict source' position and a reason for the conflict.
     * Some types of reasons also contain a digit.
     */
    public static final class Reason {
        public static final int MASK = 0xFF000000;
        public static final int MASK_DIGIT = 0x00FF0000;
        public static final int SHIFT_DIGIT = 16;
        public static final int MASK_POSITION = 0x0000FFFF;

        public static final int CONFLICT_ON_ROW = 0x01000000;
        public static final int CONFLICT_ON_COL = 0x02000000;
        public static final int CONFLICT_ON_SQR = 0x03000000;
        public static final int NO_SPACE_ON_ROW = 0x04000000; // Digit
        public static final int NO_SPACE_ON_COL = 0x05000000; // Digit
        public static final int NO_SPACE_ON_SQR = 0x06000000; // Digit
        public static final int CELL_ALREADY_WRITTEN = 0x07000000;

        public static int digitHROf(int r) {
            return ((r & MASK_DIGIT) >> SHIFT_DIGIT) + 1;
        }
    }
}
