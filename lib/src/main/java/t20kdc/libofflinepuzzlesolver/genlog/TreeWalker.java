package t20kdc.libofflinepuzzlesolver.genlog;

import java.util.concurrent.atomic.AtomicBoolean;

public interface TreeWalker<N> {
    static <N> TreeWalker<N> of(TreeWalkerRoot<N> stateTreeWalkerRoot, AtomicBoolean canceller) {
        return new TreeWalkerThreads<>(stateTreeWalkerRoot, canceller);
    }

    /**
     * Walks the tree.
     * @param init Initial state.
     * @return A success state (if any, else null)
     */
    N walk(N init);

    N getHighestImportance();
}
