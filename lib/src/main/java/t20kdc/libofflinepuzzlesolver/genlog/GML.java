package t20kdc.libofflinepuzzlesolver.genlog;

/**
 * Logging that works outside of Android for General Logic
 */
public class GML {
    // Enabled by default for LogCat retrieval.
    // Disabled during offline testing as it's much easier to introspect directly there.
    public static boolean logEnabled = true;
    public static void log(String s) {
        if (logEnabled)
            System.err.println("LOPSGL: " + s);
    }
}
