package t20kdc.libofflinepuzzlesolver.genlog;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static t20kdc.libofflinepuzzlesolver.genlog.GML.log;

/**
 * Unlike ComplexPathfinder, this can only work with trees, and has no cost measurement.
 * However, as a result, it's multi-threaded without severe synchronization and has much less overhead.
 * It is depth-first for memory usage reasons.
 * It's also non-deterministic in the presence of multiple endpoints.
 * It also can use boxed types, but it probably won't help much to.
 */
public final class TreeWalkerThreads<N> implements TreeWalker<N> {
    // The distro loop is used to distribute load across threads.
    // It uses a semaphore to establish the happens-before memory relationship.
    // Keep in mind that the distro loop is "working successfully" even with only very occasional use.
    private final Semaphore distroLoopSemaphore = new Semaphore(1);
    private volatile N distroLoopValue = null;
    private volatile N endpoint;
    // Not reliable but may be useful to give interesting failed results.
    private final AtomicReference<N> highestImportance = new AtomicReference<>();
    private final AtomicInteger nodesInPlay = new AtomicInteger();
    private final AtomicInteger threadsInPlay = new AtomicInteger();
    private final Semaphore waker = new Semaphore(1);
    private final Semaphore confirmer = new Semaphore(1);
    private final AtomicBoolean canceller;

    // Contains stuff that's been moved out of TreeWalker as part of debugging via code simplification
    private final TreeWalkerRoot<N> root;

    public TreeWalkerThreads(TreeWalkerRoot<N> r, AtomicBoolean c) {
        root = r;
        canceller = c;
    }

    private void threadContent() {
        // This is this thread's list 'o' nodes.
        List<N> myList = new ArrayList<>(1024);
        // Note that if nodesInPlay ever hits 0, we're out of tree to searcch
        while (true) {
            if (nodesInPlay.get() == 0) {
                if (myList.size() > 0) {
                    log("Thread exiting ERRONEOUSLY as tree has been cleared");
                }
                log("Thread exiting as tree has been cleared");
                distroLoopSemaphore.acquireUninterruptibly();
                if (distroLoopValue != null)
                    log("Tree clear was not correct as value stuck in distro loop");
                distroLoopSemaphore.release();
                break;
            }
            // If an endpoint has been found, just quit early
            if (endpoint != null) {
                log("Thread exiting as endpoint has been found");
                break;
            }
            N triedToGet = null;
            // Firstly, try grabbing something from the end of the array.
            int mls = myList.size();
            if (mls > 0)
                triedToGet = myList.remove(mls - 1);
            // Secondly, if that succeeded, maybe throw it on the distribution loop & restart
            if (triedToGet != null) {
                if (distroLoopSemaphore.tryAcquire()) {
                    if (distroLoopValue == null) {
                        distroLoopValue = triedToGet;
                        distroLoopSemaphore.release();
                        // Success, so do another loop
                        continue;
                    } else {
                        distroLoopSemaphore.release();
                    }
                }
            }
            // Thirdly, try grabbing something from the distribution loop.
            if (triedToGet == null) {
                if (distroLoopSemaphore.tryAcquire()) {
                    triedToGet = distroLoopValue;
                    distroLoopValue = null;
                    distroLoopSemaphore.release();
                }
            }
            // Fourth, yield.
            if (triedToGet == null) {
                Thread.yield();
            } else {
                // Process this node.
                // Endpoint & Importance
                if (root.isEndpoint(triedToGet)) {
                    confirmer.acquireUninterruptibly();
                    endpoint = triedToGet;
                    confirmer.release();
                    // we're done!
                    nodesInPlay.decrementAndGet(); // bring endpoint out of play
                    log("Thread exiting as it found the endpoint");
                    break;
                }
                N importanceCheck = highestImportance.get();
                // Note that this really *isn't* perfect, particularly if multiple generations are skipped at once.
                // Ultimately it's a heuristic to give 'failed' results.
                if ((importanceCheck == null) || (root.getImportanceOf(triedToGet) > root.getImportanceOf(importanceCheck)))
                    highestImportance.compareAndSet(importanceCheck, triedToGet);
                // Main
                int oldSize = myList.size();
                // Cancellation causes further visits to not run, which shuts down the exponential growth.
                // The system will come to a safe halt after that point.
                if (!canceller.get())
                    root.visit(triedToGet, myList);
                int newSize = myList.size();
                // The +1 accounts for the original node being removed. Must be done this late to prevent race conditions.
                nodesInPlay.addAndGet(newSize - (oldSize + 1));
            }
        }
        // We're shutting down now, pull all nodes out of play
        nodesInPlay.addAndGet(-myList.size());
        if (threadsInPlay.decrementAndGet() == 0) {
            // We're the last thread and therefore responsible for waker
            log("Releasing Waker Externally");
            waker.release();
        }
    }

    /**
     * @return An endpoint node, if any.
     */
    @Override
    public N walk(N init) {
        log("MT/Starting...");
        root.initState(init);
        // Initialize state
        distroLoopSemaphore.acquireUninterruptibly();
        distroLoopValue = init; // Distro-loop loaded with first node
        distroLoopSemaphore.release();
        endpoint = null; // No endpoint found
        highestImportance.set(null); // No importance
        nodesInPlay.set(1); // First node in play
        // just do this
        int processors = Runtime.getRuntime().availableProcessors();
        Thread[] threads = new Thread[processors];
        threadsInPlay.set(threads.length);
        waker.acquireUninterruptibly(); // Semaphore at 1 (busy)
        // Start threads
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread("TreeWalker-" + i) {
                @Override
                public void run() {
                    threadContent();
                }
            };
            threads[i].start();
        }
        log("MT/Main body...");
        // Wait for completion
        while (true) {
            boolean didAcquire = false;
            try {
                didAcquire = waker.tryAcquire(1000, TimeUnit.MILLISECONDS);
            } catch (Exception e) {
                // don't care
            }
            if (didAcquire)
                break;
            log("MT/in play: " + nodesInPlay.get());
        }
        log("MT/Shutting down...");
        // Successfully acquired, instantly release
        waker.release();
        // Acquire/release this just to be really sure
        confirmer.acquireUninterruptibly();
        confirmer.release();
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException ie) {
                // tried
                ie.printStackTrace();
            }
        }
        log("MT/All threads joined");
        return endpoint;
    }

    @Override
    public N getHighestImportance() {
        return highestImportance.get();
    }
}
