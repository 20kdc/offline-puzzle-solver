package t20kdc.libofflinepuzzlesolver.genlog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * Useful for implementing solvers.
 * It's a bit of a brute-force approach, but when used on a sufficiently constrained problem, it should result in easy to understand code that will always work reliably.
 */
public final class ComplexPathfinder<N extends ComplexPathfinder.Node<N, L>, L> {
    /**
     * The link backwards (towards the nearest start).
     * Also contains the 'start link' itself.
     * Therefore acts as a reachability check.
     */
    public final HashMap<N, CostFT<N, L>> backlinks = new HashMap<>();
    /**
     * Nodes that will be updated on next iteration.
     */
    public final HashSet<N> needsUpdate = new HashSet<N>();
    /**
     * Endpoints that have been found.
     * These might not be in exploredWithCF.
     * *In this event the path is empty, not unfindable.*
     * This indicates the start node was an endpoint.
     */
    public final HashSet<N> knownEndpoints = new HashSet<N>();

    /**
     * Use to specify a start node.
     * Note that if it's easier to just plain use a different start node than to pay the base cost of this node,
     *  this node will be ignored.
     * @param node start node.
     * @param baseCost Base cost
     * @return success
     */
    public final boolean addStart(N node, int baseCost, L link) {
        return tryApplyBacklink(new CostFT<>(null, node, baseCost, link));
    }

    /**
     * Tries to apply a backlink. Fails if the backlink would be worse than what's already there.
     * Sets up updating accordingly.
     * @param link the backlink
     * @return success
     */
    private boolean tryApplyBacklink(CostFT<N, L> link) {
        CostFT<N, L> existing = backlinks.get(link.to);
        if (existing != null)
            if (existing.totalCost <= link.totalCost)
                return false; // not useful
        if (link.to.isEndpoint())
            knownEndpoints.add(link.to);
        backlinks.put(link.to, link);
        needsUpdate.add(link.to);
        return true;
    }

    /**
     * Propagates by one unit.
     * @return amount of added links
     */
    public final int iterate() {
        int res = 0;
        ArrayList<N> aln = new ArrayList<>(needsUpdate);
        needsUpdate.clear();
        for (N v : aln) {
            CostFT<N, L> cf = backlinks.get(v);
            for (CostFT<N, L> ct : cf.to.costTo(cf.totalCost))
                if (tryApplyBacklink(ct))
                    res++;
        }
        return res;
    }

    /**
     * Checks for a path to any valid start node given a destination node.
     * Note that this will include the start link.
     * Therefore [0].to will be the start node, and the last element's to will be the destination node.
     * @param to start node
     * @return the path
     */
    public final CostFT<N, L>[] checkForPath(N to) {
        CostFT<N, L> res = backlinks.get(to);
        if (res == null)
            return null;
        LinkedList<CostFT<N, L>> nodes = new LinkedList<>();
        while (res != null) {
            nodes.addFirst(res);
            // hit start node, just break immediately...
            if (res.from == null)
                break;
            res = backlinks.get(res.from);
        }
        return GA.ofLL(nodes, CostFT.class);
    }

    /**
     * Runs until the endpoint node has a path or iteration cannot continue.
     * Stops as soon as any endpoint node has a path, regardless of cost.
     * @return A path, if any. Can be empty if the starting node was an endpoint.
     */
    public final CostFT<N, L>[] runToCompletion() {
        while (knownEndpoints.isEmpty()) {
            if (!needsUpdate.isEmpty()) {
                iterate();
            } else {
                // nothing we can do
                return null;
            }
        }
        for (N node : knownEndpoints) {
            CostFT<N, L>[] path = checkForPath(node);
            // failing to get a path this way shouldn't happen
            if (path != null)
                return path;
        }
        return null;
    }

    /**
     * Implementations must be equatable and hashcodable with respect to other nodes in the same pathfinder instance.
     * Note that this means nodes need not check or hash state that would be only used by other pathfinder instances.
     * @param <N> Itself
     * @param <L> Link annotation (or whatever)
     */
    public interface Node<N, L> {
        CostFT<N, L>[] costTo(int baseCost);

        /**
         * @return If the node is an endpoint (pathfinding ought to stop here).
         */
        boolean isEndpoint();
    }

    public final static class CostFT<N, L> {
        public final N from;
        public final N to;
        public final int totalCost;
        public final L link;
        public CostFT(N n, N nt, int c, L ln) {
            from = n;
            to = nt;
            totalCost = c;
            link = ln;
        }
    }
}
