package t20kdc.libofflinepuzzlesolver.genlog;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The simplest possible tree walker.
 */
public class TreeWalkerSimple<N> implements TreeWalker<N> {
    private final TreeWalkerRoot<N> root;
    private final AtomicBoolean canceller;
    private N highestImportance;

    public TreeWalkerSimple(TreeWalkerRoot<N> r, AtomicBoolean c) {
        root = r;
        canceller = c;
    }

    @Override
    public N walk(N init) {
        root.initState(init);
        highestImportance = null;
        return walkI(init);
    }

    private N walkI(N init) {
        if (canceller.get())
            return null;
        if (root.isEndpoint(init))
            return init;
        if (highestImportance == null) {
            highestImportance = init;
        } else if (root.getImportanceOf(highestImportance) < root.getImportanceOf(init)) {
            highestImportance = init;
        }
        ArrayList<N> al = new ArrayList<>(16);
        root.visit(init, al);
        for (N v : al) {
             N res = walkI(v);
             if (res != null)
                 return res;
        }
        return null;
    }

    @Override
    public N getHighestImportance() {
        return highestImportance;
    }
}
