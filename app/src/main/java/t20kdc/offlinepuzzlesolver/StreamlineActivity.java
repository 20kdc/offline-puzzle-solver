package t20kdc.offlinepuzzlesolver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

import t20kdc.offlinepuzzlesolver.game.Solver;

/**
 * Base Activity class for the "specify problem -> solve -> done loop".
 * In particular, handles deferring the solving to a thread.
 */
public abstract class StreamlineActivity extends Activity {
    public final void loadSL(Bundle savedInstanceState) {
        Streamline sl = new Streamline(this, savedInstanceState, false);
        onStreamline(sl);
        sl.finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Streamline sl = new Streamline(this, savedInstanceState, true);
        onStreamline(sl);
        sl.finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Streamline sl = new Streamline(this, null, true);
        onStreamline(sl);
        sl.finish();
    }

    public abstract void onStreamline(Streamline streamline);

    public void bindSolveButton(Class<?> cs) {
        Log.println(Log.INFO, "StreamlineActivity", "Solve button binding (class " + cs.toString() + ")");
        findViewById(R.id.btn_solve).setOnClickListener((b) -> {
            Log.println(Log.INFO, "StreamlineActivity", "Solve button pressed (class " + cs.toString() + ")");
            final Bundle bundle = new Bundle();
            Streamline sl = new Streamline(this, bundle, true);
            onStreamline(sl);
            sl.finish();
            final AtomicBoolean canceller = new AtomicBoolean();
            WorkingActivity.launch(this, new PuzzleSolverAsyncThread(cs, bundle, canceller, this), canceller);
        });
    }
    public void bindRulesButton(String base) {
        findViewById(R.id.btn_rules).setOnClickListener((b) -> {
            Intent i = new Intent(this, SolutionActivity.class);
            i.putExtra(Intent.EXTRA_TITLE, getString(R.string.rules_title));
            i.putExtra("t20kdc.top.pipe", base);
            startActivity(i);
        });
    }

    private static class PuzzleSolverAsyncThread extends Thread {
        private final Class<?> cs;
        private final Bundle bundle;
        private final AtomicBoolean canceller;
        private final WeakReference<StreamlineActivity> activity;

        public PuzzleSolverAsyncThread(Class<?> cs, Bundle bundle, AtomicBoolean canceller, StreamlineActivity wr) {
            super("PuzzleSolverAsync");
            this.cs = cs;
            this.bundle = bundle;
            this.canceller = canceller;
            activity = new WeakReference<>(wr);
        }

        @Override
        public void run() {
            String decidedText = "?";
            try {
                Solver s = (Solver) cs.newInstance();
                StringBuilder text = new StringBuilder();
                try {
                    s.solve(bundle, text, canceller);
                } catch (Exception e) {
                    text.append("<code>AN EXCEPTION OCCURRED...\n");
                    StringWriter sw = new StringWriter();
                    e.printStackTrace(new PrintWriter(sw));
                    text.append(sw.toString());
                    text.append("</code>");
                }
                // Check this both on the UI thread and here, just to be certain.
                // It would be bad to activate an unrequested Activity.
                if (canceller.get())
                    return;
                decidedText = text.toString();
            } catch (Exception e) {
                e.printStackTrace();
                decidedText = "Exterior solver management exception. This shouldn't happen. As reporting the last exception could've caused the problem in the first place, no exception stack trace will be provided. Please defer to LogCat.";
            }
            final String decidedTextFinal = decidedText;
            // Delay this to the last possible moment to prevent leaking StreamlineActivity.
            // For further context, please see cuixiaoyiyi's issue here:
            // https://gitlab.com/20kdc/offline-puzzle-solver/-/issues/1
            final StreamlineActivity sa = activity.get();
            if (sa == null)
                return;
            sa.runOnUiThread(() -> {
                if (canceller.get())
                    return;
                Intent i = new Intent(sa, SolutionActivity.class);
                i.putExtra(Intent.EXTRA_HTML_TEXT, decidedTextFinal);
                sa.startActivity(i);
            });
        }
    }
}
