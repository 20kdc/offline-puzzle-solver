package t20kdc.offlinepuzzlesolver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Activity used while the device is busy running a solver.
 */
public class WorkingActivity extends Activity {
    // **Both of these fields are only set by the UI thread!**
    // If this thread is dead, the activity will be auto-closed.
    public static Thread currentWorkerThread;
    // **Both of these fields are only set by the UI thread!**
    // Contents set to true if the activity is closed. Used to shut down threads.
    // Note that this being set to true when the WorkingActivity has not been finished will cause an apparent hang.
    // DO NOT DO THIS.
    public static AtomicBoolean currentCanceller;

    public static void launch(StreamlineActivity streamlineActivity, Thread thread, AtomicBoolean canceller) {
        currentWorkerThread = thread;
        currentCanceller = canceller;
        thread.start();
        streamlineActivity.startActivity(new Intent(streamlineActivity, WorkingActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_working);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Nope. We're done.
        if ((currentWorkerThread == null) || (!currentWorkerThread.isAlive()))
            finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            if (currentCanceller != null) {
                Log.println(Log.INFO, "WorkingActivity", "User has finished WorkingActivity, and a canceller is available");
                currentCanceller.set(true);
            } else {
                Log.println(Log.INFO, "WorkingActivity", "User has finished WorkingActivity, and a canceller is not available");
            }
        }
    }
}