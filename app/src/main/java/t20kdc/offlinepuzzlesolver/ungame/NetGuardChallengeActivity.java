package t20kdc.offlinepuzzlesolver.ungame;

import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.security.MessageDigest;

import t20kdc.offlinepuzzlesolver.R;
import t20kdc.offlinepuzzlesolver.Streamline;
import t20kdc.offlinepuzzlesolver.StreamlineActivity;

/**
 * Oh, you're wondering about this, aren't you?
 */
public class NetGuardChallengeActivity extends StreamlineActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_net_guard_challenge);
        ((EditText) findViewById(R.id.challenge)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                regenerate(editable.toString());
            }
        });
        findViewById(R.id.btn_copy).setOnClickListener((b) -> {
            TextView x = (TextView) findViewById(R.id.solution);
            ((ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE)).setText(x.getText());
        });
        findViewById(R.id.btn_paste).setOnClickListener((b) -> {
            CharSequence text = ((ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE)).getText();
            if (text != null)
                ((EditText) findViewById(R.id.challenge)).setText(text);
        });
        findViewById(R.id.ng_compat).setOnClickListener((b) -> {
            regenerate(((EditText) findViewById(R.id.challenge)).getText().toString());
        });
        loadSL(savedInstanceState);
    }

    @Override
    public void onStreamline(Streamline sl) {
        // order is important because ng_compat reacts to click, it shouldn't but CBA
        sl.interact("ng_compat", R.id.ng_compat, false);
        sl.interact("ng_challenge", R.id.challenge, Build.SERIAL);
    }

    // Provides a new answer.
    private void regenerate(String str) {
        TextView x = (TextView) findViewById(R.id.solution);
        try {
            // challenge contains a serial number, and gets an "O3" prefix if it's on a new Android version w/ a different mechanism
            // however, the ID could in theory have O3 at the start
            // work around this
            boolean guess = str.startsWith("O3");
            if (((CheckBox) findViewById(R.id.ng_compat)).isChecked())
                guess = !guess;
            if (guess) {
                str += "NetGuard3";
            } else {
                str += "NetGuard2";
            }
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] res = md.digest(str.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < res.length; i++)
                sb.append(String.format("%02X", res[i]));
            x.setText(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            x.setText("ERROR: " + e.toString() + " ; CHECK LOGS");
        }
    }
}