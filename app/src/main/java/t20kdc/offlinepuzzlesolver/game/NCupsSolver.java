package t20kdc.offlinepuzzlesolver.game;

import android.os.Bundle;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

import t20kdc.libofflinepuzzlesolver.genlog.ComplexPathfinder;
import t20kdc.libofflinepuzzlesolver.genlog.GA;

public class NCupsSolver implements Solver {
    @Override
    public void solve(Bundle b, StringBuilder sol, AtomicBoolean canceller) {
        int cups = b.getInt("seek_cup_count") + 1;
        int move = b.getInt("seek_cup_move") + 1;
        boolean[] cupStates = new boolean[cups];
        // This value is basically the core of the actual solver.
        int downCupCount = 0;
        for (int i = 0; i < cups; i++) {
            cupStates[i] = b.getInt("cup_" + i) != 0;
            if (cupStates[i])
                downCupCount++;
        }
        // Firstly, setup and run pathfinding.
        NCupsNode here = new NCupsNode(cups, move, downCupCount);
        ComplexPathfinder<NCupsNode, Object> pathfinder = new ComplexPathfinder<>();
        pathfinder.addStart(here, 0, null);
        pathfinder.runToCompletion();
        // Secondly, attempt to determine the 'best we can do'.
        int resultDown = downCupCount;
        ComplexPathfinder.CostFT<NCupsNode, Object>[] resultPath = null;
        for (int i = 0; i <= resultDown; i++) {
            NCupsNode target = new NCupsNode(cups, move, i);
            resultPath = pathfinder.checkForPath(target);
            if (resultPath != null) {
                resultDown = i;
                break;
            }
        }
        // Thirdly, extrapolate that from deltas into cup instructions.
        LinkedList<Integer> allFlips = null;
        if (resultPath != null)
            allFlips = pathIntoFlips(move, cupStates, resultPath, sol);
        // Fourth, format the results.
        formatResults(sol, move, cupStates, resultPath, allFlips, resultDown);
    }

    private LinkedList<Integer> pathIntoFlips(int move, boolean[] cupStates, ComplexPathfinder.CostFT<NCupsNode, Object>[] resultPath, StringBuilder logger) {
        LinkedList<Integer> allFlips = new LinkedList<>();
        for (int i = 1; i < resultPath.length; i++) {
            // ABOVE ALL ELSE:
            // Note that the only reason this is stable is because the pathfinder will only ever give achievable moves.
            int oldCount = resultPath[i].from.downCupCount;
            int newCount = resultPath[i].to.downCupCount;
            // Extrapolate into 'down turned up' and 'up turned down' cups...
            int upIntoDown;
            int downIntoUp;
            if (oldCount > newCount) {
                // mainly down -> up
                downIntoUp = oldCount - newCount;
                upIntoDown = 0;
            } else {
                // mainly up -> down
                upIntoDown = newCount - oldCount;
                downIntoUp = 0;
            }
            // Pad as required to meet move requirement.
            while ((upIntoDown + downIntoUp) < move) {
                downIntoUp++;
                upIntoDown++;
            }
            // Iterate and add moves...
            int applied = 0;
            int outDown = 0;
            boolean[] newCupStates = dupCupStates(cupStates);
            for (int j = 0; j < newCupStates.length; j++) {
                if ((downIntoUp > 0) && newCupStates[j]) {
                    allFlips.add(j);
                    downIntoUp--;
                    applied++;
                    newCupStates[j] = false;
                } else if ((upIntoDown > 0) && !newCupStates[j]) {
                    allFlips.add(j);
                    upIntoDown--;
                    applied++;
                    newCupStates[j] = true;
                }
                if (newCupStates[j])
                    outDown++;
            }
            boolean failure = false;
            if (applied != move) {
                logger.append("<p><i>ERROR: calculated " + applied + " flips in a move, which is not allowed.</i></p>");
                failure = true;
            } else if (outDown != newCount) {
                logger.append("<p><i>ERROR: simulated output did not actually match pathfinding.</i></p>");
                failure = true;
            }
            if (failure) {
                logger.append("<p><i>this is probably a bug, but may be the kind that indicates the theory behind all this doesn't really work.</i></p>");
                logger.append("<p><i>see if the solution summary makes sense.</i></p>");
                logger.append("<p><i>precursor of move was " + formatCups(cupStates) + ".</i></p>");
                logger.append("<p><i>broken result of move was " + formatCups(newCupStates) + ".</i></p>");
                logger.append("<p><i>move old/new was " + oldCount + " : " + newCount + ".</i></p>");
                logger.append("<p><i>remainder uid/diu " + upIntoDown + " : " + downIntoUp + ".</i></p>");
                return null;
            }
            cupStates = newCupStates;
        }
        return allFlips;
    }

    private void formatResults(StringBuilder sol, int move, boolean[] initCupStates, ComplexPathfinder.CostFT<NCupsNode, Object>[] resultPath, LinkedList<Integer> allFlips, int down) {
        sol.append("<h1>Results</h1>");
        if (down != 0) {
            sol.append("<p>Failed: A solution couldn't be determined that reduced the number of down cups to 0. The closest will be given.</p>");
        } else {
            sol.append("<p>Success: A solution was found.</p>");
        }
        sol.append("<h1>Input</h1>");
        sol.append("<ul>");
        sol.append("<li>Cups: " + initCupStates.length + "</li>");
        sol.append("<li>Amount flipped per move: " + move + "</li>");
        sol.append("<li>Cups down: " + formatCups(initCupStates) + "</li>");
        sol.append("</ul>");
        boolean[] cupStates = dupCupStates(initCupStates);
        if (allFlips != null) {
            sol.append("<h1>Solution</h1>");
            sol.append("<ol>");
            for (int i = 0; i < allFlips.size(); i += move) {
                sol.append("<li><ul>");
                for (int j = 0; j < move; j++) {
                    int idx = allFlips.get(i + j);
                    sol.append("<li>Flip cup " + (idx + 1) + ".</li>");
                    cupStates[idx] = !cupStates[idx];
                }
                sol.append("<li>Result: " + formatCups(cupStates) + "</li>");
                sol.append("</ul></li>");
            }
            if (down == 0) {
                sol.append("<li>Success (all cups will be upright)</li>");
            } else {
                sol.append("<li>Fail (" + down + " down)</li>");
            }
            sol.append("</ol>");
        }
        sol.append("<h1>Solution Summary</h1>");
        if (resultPath != null) {
            sol.append("<pre>\n");
            if (allFlips == null) {
                sol.append("Due to some sort of error, the per-cup moves and the changes caused by them are not available.\n");
                sol.append("However, the amount of cups flipped is still available.\n");
            }
            // reset cup states
            System.arraycopy(initCupStates, 0, cupStates, 0, cupStates.length);
            boolean[] isFlipped = new boolean[initCupStates.length];
            int afp = 0;
            for (int i = 0; i < resultPath.length; i++) {
                if (allFlips != null) {
                    if (i != 0) {
                        sol.append("  ");
                        for (int j = 0; j < move; j++)
                            isFlipped[allFlips.get(afp++)] = true;
                        for (int j = 0; j < cupStates.length; j++) {
                            sol.append(isFlipped[j] ? "X" : " ");
                            if (isFlipped[j]) {
                                isFlipped[j] = false;
                                cupStates[j] = !cupStates[j];
                            }
                        }
                        sol.append("\n");
                    }
                    sol.append("+ ");
                    sol.append(formatCupsI(cupStates));
                    sol.append(" ");
                } else {
                    sol.append("+ ");
                }
                sol.append(resultPath[i].to.downCupCount + " down.\n");
            }
            sol.append("</pre>\n");
        } else {
            sol.append("<p>Not available (error).</p>");
        }
    }

    private boolean[] dupCupStates(boolean[] cupStates) {
        boolean[] bl = new boolean[cupStates.length];
        System.arraycopy(cupStates, 0, bl, 0, cupStates.length);
        return bl;
    }

    private String formatCupsI(boolean[] initCupStates) {
        char[] ch = new char[initCupStates.length];
        for (int i = 0; i < ch.length; i++)
            ch[i] = initCupStates[i] ? 'D' : 'U';
        return new String(ch);
    }

    private String formatCups(boolean[] initCupStates) {
        int downAtEnd = 0;
        for (int i = 0; i < initCupStates.length; i++)
            if (initCupStates[i])
                downAtEnd++;
        return "<code>" + formatCupsI(initCupStates) + "</code> (" + downAtEnd + " down)";
    }

    /**
     * A node for NCups solver pathfinding.
     * Note that the downCupCount values are used to calculate the 'formal' solution after path-finding.
     */
    private static final class NCupsNode implements ComplexPathfinder.Node<NCupsNode, Object> {
        // config
        public final int cups;
        public final int move;
        // actual
        public final int downCupCount;
        public NCupsNode(int c, int m, int dcc) {
            cups = c;
            move = m;
            downCupCount = dcc;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof NCupsNode))
                return false;
            return downCupCount == ((NCupsNode) o).downCupCount;
        }

        @Override
        public int hashCode() {
            return downCupCount;
        }

        @Override
        public ComplexPathfinder.CostFT<NCupsNode, Object>[] costTo(int baseCost) {
            // To understand this logic, firstly, imagine all the cups are in a line,
            // and on the left side is all the 'down' cups, and on the right side is all the 'not down' cups.
            // Now imagine a 'sliding window' of size (cups in a move) going across the line.
            LinkedList<Integer> dcc = new LinkedList<>();
            for (int i = 0; i <= (cups - move); i++) {
                int resDCC = downCupCount;
                if (i <= downCupCount - move)
                    resDCC -= move; // whole disable
                else if (i >= downCupCount)
                    resDCC += move; // whole enable
                else {
                    // these are the complex moves that actually achieve useful, non-obvious effects
                    // keep in mind that no range checks are required, those are done above.
                    // at least one cup in range is not down and at least one cup is down, etc.
                    // however, they also work at the very edges (where values end up 0), even though this never occurs -
                    // this helps get an idea of how they work.
                    // start of window to downCupCount gets flipped upwards
                    resDCC -= downCupCount - i;
                    // downCupCount to end of window gets flipped downwards
                    resDCC += (i + move) - downCupCount;
                }
                dcc.add(resDCC);
            }
            ComplexPathfinder.CostFT<NCupsNode, Object>[] res = GA.of(new ComplexPathfinder.CostFT[dcc.size()]);
            for (int i = 0; i < res.length; i++) {
                int ndcc = dcc.get(i);
                res[i] = new ComplexPathfinder.CostFT<>(this, new NCupsNode(cups, move, ndcc), baseCost + 1, null);
            }
            return res;
        }

        @Override
        public boolean isEndpoint() {
            return downCupCount == 0;
        }
    }
}
