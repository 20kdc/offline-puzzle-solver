package t20kdc.offlinepuzzlesolver.game;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

import t20kdc.libofflinepuzzlesolver.genlog.TreeWalker;
import t20kdc.libofflinepuzzlesolver.genlog.TreeWalkerRoot;
import t20kdc.libofflinepuzzlesolver.sudoku.SudokuRoot;

/**
 * Fun fact.
 * This class used to be nice and logical and also efficient.
 * BUT THEN IT DIDN'T WORK.
 */
public class SudokuSolver implements Solver {
    @Override
    public void solve(Bundle b, StringBuilder sol, AtomicBoolean canceller) {
        // Load state...
        SudokuRoot.State st = new SudokuRoot.State();
        for (int i = 0; i < 9 * 9; i++) {
            int v = b.getInt("c" + i);
            if (v != 0) {
                if (st.wouldBeValid(i, v - 1)) {
                    st = new SudokuRoot.State(st, i, v - 1);
                } else {
                    int r = st.reasonFor(i, v - 1);
                    sol.append("<i>The input contained a contradiction preventing tile " + ((i % 9) + 1) + ", " + ((i / 9) + 1) + " from being placed. Reason follows.</i><br/>");
                    drawReason(r, st, sol, i);
                    return;
                }
            }
        }
        // Check for epic failure
        int ef = st.checkForEpicFailure();
        if (ef != 0) {
            sol.append("<i>The input puzzle was unsolvable in a clear fashion. Reason follows.</i><br/>");
            drawReason(ef, st, sol, -1);
            return;
        }
        // Run
        TreeWalker<SudokuRoot.State> tw = TreeWalker.of(new SudokuRoot(), canceller);
        SudokuRoot.State res = tw.walk(st);
        boolean failed = false;
        if (res == null) {
            sol.append("<p>Failed.</p>");
            sol.append("<p>But here's the best shot anyway...</p>");
            st.enableWalkabouts = true;
            tw.walk(st);
            res = tw.getHighestImportance();
            sol.append("<h1>Closest</h1>");
        } else {
            sol.append("<h1>Solution</h1>");
        }
        if (res != null) {
            sol.append("<h2>Main</h2>");
            drawBoard(sol, res, -1, -1);
            sol.append("<h2>Formatted for copying</h2>");
            sol.append("<pre>\n");
            for (int y = 0; y < 9; y++) {
                if ((y % 3) == 0)
                    sol.append('\n');
                for (int x = 0; x < 9; x++) {
                    if ((x % 3) == 0)
                        sol.append(' ');
                    char ch = '?';
                    int choice = res.getChoice(x + (y * 9));
                    if (choice != -1)
                        ch = (char) ('1' + choice);
                    sol.append(ch);
                }
                sol.append('\n');
            }
            sol.append("</pre>\n");
            if (failed) {
                // check if a reason is immediately apparent
                int r2 = res.checkForEpicFailure();
                if (r2 != 0) {
                    sol.append("<h2>Interesting Note About This Failure</h2>");
                    drawReason(r2, res, sol, -1);
                }
            }
        }
    }

    private void drawBoard(StringBuilder sol, SudokuRoot.State res, int importantIndexDark, int importantIndexLight) {
        sol.append("<table border=\"1\" style=\"font-family: monospace;\">");
        for (int y = 0; y < 9; y++) {
            if ((y != 0) && ((y % 3) == 0))
                sol.append("<tr><td></td></tr>");
            sol.append("<tr>");
            for (int x = 0; x < 9; x++) {
                if ((x % 3) == 0)
                    sol.append("<td></td>");
                char ch = '?';
                int where = x + (y * 9);
                int choice = res.getChoice(where);
                String colour = "";
                if (choice != -1) {
                    ch = (char) ('1' + choice);
                } else {
                    colour = "color: transparent;";
                }
                if (importantIndexLight == where)
                    colour = "color: black; background: lightsalmon;";
                else if (importantIndexDark == where)
                    colour = "color: white; background: midnightblue;";
                sol.append("<td style=\"");
                sol.append(colour);
                sol.append("\">");
                sol.append(ch);
                sol.append("</td>");
            }
            sol.append("</tr>");
        }
        sol.append("</table>");
    }

    private void drawReason(int r, SudokuRoot.State st, StringBuilder sol, int importantIndexLight) {
        int rc = r & SudokuRoot.Reason.MASK;
        int rp = r & SudokuRoot.Reason.MASK_POSITION;
        if (rc == SudokuRoot.Reason.CONFLICT_ON_ROW)
            sol.append("<i>The lighter marked tile would conflict with the darkened row.</i>");
        else if (rc == SudokuRoot.Reason.CONFLICT_ON_COL)
            sol.append("<i>The lighter marked tile would conflict with the darkened column.</i>");
        else if (rc == SudokuRoot.Reason.CONFLICT_ON_SQR)
            sol.append("<i>The lighter marked tile would conflict with the darkened 3x3 square.</i>");
        else if (rc == SudokuRoot.Reason.NO_SPACE_ON_ROW)
            sol.append("<i>It is impossible to place digit " + SudokuRoot.Reason.digitHROf(r) + " on the marked row.</i>");
        else if (rc == SudokuRoot.Reason.NO_SPACE_ON_COL)
            sol.append("<i>It is impossible to place digit " + SudokuRoot.Reason.digitHROf(r) + " on the marked column.</i>");
        else if (rc == SudokuRoot.Reason.NO_SPACE_ON_SQR)
            sol.append("<i>It is impossible to place digit " + SudokuRoot.Reason.digitHROf(r) + " on the marked 3x3 square.</i>");
        else {
            // impossible reasons are assigned here.
            sol.append("<i>Reason code " + Integer.toHexString(r) + " ; please see light-marked and darkened tiles and source. You should never see this.</i>");
        }
        drawBoard(sol, st, r & 0xFFFF, importantIndexLight);
    }
}
