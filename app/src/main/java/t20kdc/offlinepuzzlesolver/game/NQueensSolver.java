package t20kdc.offlinepuzzlesolver.game;

import android.os.Bundle;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

import t20kdc.libofflinepuzzlesolver.genlog.TreeWalker;
import t20kdc.libofflinepuzzlesolver.genlog.TreeWalkerRoot;

public class NQueensSolver implements Solver {
    @Override
    public void solve(Bundle b, StringBuilder sol, AtomicBoolean canceller) {
        TreeWalker<State> tw = TreeWalker.of(new TreeWalkerRoot<State>() {
            @Override
            public void initState(State init) {
            }
            @Override
            public void visit(State node, Collection<State> intern) {
                int targ = node.filled;
                for (int i = 0; i < node.queenStates.length; i++) {
                    int diagU = i;
                    int diagD = i;
                    int dX = targ;
                    boolean success = true;
                    for (int j = 0; j < targ; j++) {
                        diagD++;
                        diagU--;
                        dX--;
                        if ((node.queenStates[dX] == diagU) || (node.queenStates[dX] == i) || (node.queenStates[dX] == diagD)) {
                            success = false;
                            break;
                        }
                    }
                    if (success) {
                        intern.add(new State(node, i));
                    }
                }
            }
            @Override
            public int getImportanceOf(State node) {
                return node.filled;
            }
            @Override
            public boolean isEndpoint(State node) {
                return node.filled == node.queenStates.length;
            }
        }, canceller);
        State res = tw.walk(new State(b.getInt("board_size") + 1));
        if (res == null) {
            sol.append("<i>It broke and failed to find a solution! Is this even possible?</i>");
        } else {
            sol.append("<h1>Solution</h1>");
            sol.append("<h1>Solution (Copyable)</h1>");
            sol.append("<pre>\n");
            sol.append(' ');
            for (int i = 0; i < res.queenStates.length; i++)
                sol.append('+');
            sol.append('\n');
            for (int i = 0; i < res.queenStates.length; i++) {
                sol.append('+');
                for (int j = 0; j < res.queenStates.length; j++) {
                    boolean hasQueen = res.queenStates[j] == i;
                    sol.append(hasQueen ? 'Q' : ' ');
                }
                sol.append('\n');
            }
            sol.append("</pre>\n");
        }
    }
    public static class State {
        public final int[] queenStates;
        public final int filled;
        public State(int i) {
            queenStates = new int[i];
            filled = 0;
            Arrays.fill(queenStates, -1);
        }
        public State(State previous, int i) {
            queenStates = previous.queenStates.clone();
            filled = previous.filled + 1;
            queenStates[previous.filled] = i;
        }
    }
}
