package t20kdc.offlinepuzzlesolver.game;

import android.os.Bundle;

import java.util.concurrent.atomic.AtomicBoolean;

public interface Solver {
    /**
     * Given an input problem, outputs the resulting HTML.
     * @param b Problem input.
     * @param sol Solution output.
     * @param canceller Canceller. If activated, the solution is NOT necessarily valid in any capacity. The solver will try to shut down.
     */
    void solve(Bundle b, StringBuilder sol, AtomicBoolean canceller);
}
